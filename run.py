# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from AppCode import create_app, extra_config

app = create_app()

if __name__ == '__main__':
    # 启动Web Server
    app.run(**extra_config.RUN_CONFIG)