# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from . import (
    admin,
    template_path
)

from flask import (
    render_template,
)


__all__ = []

@admin.route(r'/', methods=['GET', 'POST'])
def index():
    ''''''
    args = {
        'user': {
            'is_admin': True
        }
    }
    return render_template(template_path('pages/index.jinja2'), **args)