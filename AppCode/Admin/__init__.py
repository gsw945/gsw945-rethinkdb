# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask import (
    Blueprint,
    current_app
)

from ..dbhelper import db_conn


admin = Blueprint(
    'admin',
    __name__,
    static_folder='../static',
    template_folder='../templates'
)

MODULE_NAME = 'admin'
TEMPLATE_FOLDER = 'admin'

def log(msg, level='debug'):
    getattr(current_app.loger, level)(msg, MODULE_NAME)

def getExcept():
    return current_app.loger.getExcept()

def logExcept():
    ex_msg = getExcept()
    log(ex_msg, 'error')

def template_path(template_file):
    return '{0}/{1}'.format(
        TEMPLATE_FOLDER,
        template_file
    )

@admin.context_processor
def pre_process():
    _upl = current_app.cache.get('users_public_list')
    return {
        'MODULE': MODULE_NAME
    }