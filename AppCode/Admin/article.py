# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from . import (
    admin,
    template_path,
    db_conn
)

from flask import (
    request,
    render_template
)

from ..dbhelper.article import Article


__all__ = []

@admin.route(r'/article/list/', methods=['GET', 'POST'])
def article_list():
    ''''''
    obj = Article()
    db_result = obj.query()
    args = {
        'articles': db_result
    }
    return render_template(template_path('pages/article/list.jinja2'), **args)

@admin.route(r'/article/edit', methods=['GET', 'POST'])
@admin.route(r'/article/edit/<regex("([0-9a-f]{4}([0-9a-f]{4}-){4}[0-9a-f]{12})?"):_id>', methods=['GET', 'POST'])
def article_edit(_id=None):
    ''''''
    if not bool(_id):
        _id = request.values.get('id', None)
    args = {
        'article': None
    }
    if bool(_id):
        obj = Article()
        db_result = obj.query(_id)
        if bool(db_result) and isinstance(db_result, dict):
            args['article'] = db_result
    return render_template(template_path('pages/article/edit.jinja2'), **args)