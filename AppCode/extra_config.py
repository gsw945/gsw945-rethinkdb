# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

'''注意: 配置项可以使用以下类型:
字符串: str
整数: int
浮点数: float
字典: dict
'''

LISTEN_PORT = 9145

# degut-test-begin
TEST = {
    't1': {
        't11': {
            't111': 'hello'
        },
        't12': 'world'
    },
    't2': '^_^'
}
# degut-test-end

RUN_CONFIG = {
    'debug': True,
    'host': '0.0.0.0',
    'port': LISTEN_PORT
}

SMTP_CONFIG = {
    'server': 'smtp.163.com',
    'port': 25,
    'username': 'u_gsw_test@163.com',
    'password': 'p0gsw0test',
    'debug': False
}

PLATFORM = {
    'NAME': 'gsw945',
    'SERVER': '{0}:{1}'.format(RUN_CONFIG['host'], LISTEN_PORT),
    'VISIT_BASE': 'http://127.0.0.1:{0}'.format(LISTEN_PORT)
}

LOG_FOLDER = '{0}'.format(os.path.join(os.getcwd(), 'log'+os.sep))

STORE_IMAGE_FOLDER = '{0}'.format(os.path.join(os.getcwd(), 'store_image'+os.sep))

SERVER_VERSION_FILE = '{0}'.format(os.path.join(os.getcwd(), 'server.version'))

SERVER_SECRET_FILE = '{0}'.format(os.path.join(os.getcwd(), 'server.secret'))