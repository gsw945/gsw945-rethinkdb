# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from . import (
    visit,
    template_path
)

from flask import (
    request,
    render_template
)

from ..dbhelper.article import Article

__all__ = []

@visit.route(r'/article/<regex("([0-9a-f]{4}([0-9a-f]{4}-){4}[0-9a-f]{12})?"):_id>', methods=['GET', 'POST'])
def article_view(_id=None):
    ''''''
    if not bool(_id):
        _id = request.values.get('id', None)
    args = {
        'article': None
    }
    if bool(_id):
        obj = Article()
        db_result = obj.query(_id)
        if bool(db_result) and isinstance(db_result, dict):
            args['article'] = db_result
    return render_template(template_path('pages/article/view.jinja2'), **args)

@visit.route(r'/article/', methods=['GET', 'POST'])
def article_list():
    ''''''
    obj = Article()
    db_result = obj.query()
    args = {
        'articles': db_result
    }
    return render_template(template_path('pages/article/list.jinja2'), **args)