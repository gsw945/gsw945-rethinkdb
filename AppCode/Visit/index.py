# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from . import (
    visit,
    template_path
)

from flask import (
    render_template,
)


__all__ = []

@visit.route(r'/', methods=['GET', 'POST'])
def index():
    ''''''
    args = {}
    return render_template(template_path('pages/index.jinja2'), **args)