/*************begin of array.js***************/
/**
 * description: extend some function for Array,not using Array.prototype for avoiding unexpected question for using for-in loop
 */
/**
 * 数组辅助功能对象定义
 */
var array={};
/**
 * 判断给定的对象是否是有效的数组
 */
array.is=function(arr) {
    return (typeof(arr)=='object'&&arr!=null&&(arr instanceof Array));
};
/**
 * 删除数组中指定元素
 */
array.remove=function(arr,obj) {
    if(!array.is(arr)) {
        console.error('parameter "arr" is not a effective Array instance');
        return;
    }
    var _index;
    for(_index=0;_index<arr.length;_index++) {
        if(arr[_index]==obj) {
            break;
        }
    }
    if(_index>=0&&_index<arr.length) {
         arr.splice(_index, 1);
    }
};
/**
 * 移除数组中指定索引的元素
 */
array.removeAt=function(arr,index) {
    if(!array.is(arr)) {
        console.error('parameter "arr" is not a effective Array instance');
        return;
    }
    arr.splice(index,1);
};
/**
 * 清空数组
 */
array.clear=function(arr) {
    if(!array.is(arr)) {
        console.error('parameter "arr" is not a effective Array instance');
        return;
    }
    arr.splice(0,arr.length);
};
/**
 * 得到数组的最后一个元素
 */
array.last=function(arr) {
    if(!array.is(arr)) {
        console.error('parameter "arr" is not a effective Array instance');
        return;
    }
    var newArr=arr.slice(-1,arr.length);
    return newArr.length>0?newArr[0]:undefined;
};
/**
 * 获取数组中某个元素的索引(不存在时返回-1)
 */
array.indexOf=function(arr,elem) {
    if(!array.is(arr)) {
        console.error('parameter "arr" is not a effective Array instance');
        return undefined;
    }
    console.log('arr: ',arr);
    console.log('elem: ',elem);
    for(var _i=0;_i<arr.length;_i++) {
        if(JS.equals(arr[_i],elem)) {
            return _i;
        }
    }
    return -1;
};
/**
 * 得到数组的第一个元素
 */
array.first=function(arr) {
    if(!array.is(arr)) {
        console.error('parameter "arr" is not a effective Array instance');
        return;
    }
    var newArr=arr.slice(0,1);
    return newArr.length>0?newArr[0]:undefined;
};
/**
 * 判断数组中是否含有某元素
 */
array.has=function(arr,elem) {
    var isHas=array.indexOf(arr,elem);
    return isHas!=undefined&&isHas>-1;
};
/************end of array.js****************/