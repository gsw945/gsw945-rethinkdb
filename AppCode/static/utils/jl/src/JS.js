/***************begin of JS.js*****************/
/**
 * description: JavaScript Scaffold
 */
/**
 * JavaScript脚手架定义
 */
var JS={};
/**
 * 
 */
JS.types=['Null','Undefined','Number','Boolean','String','Function','Date','Math','Array','RegExp','Object','Error','JSON','Arguments'];
/**
 * 考虑到兼容性，实现去掉字符串两端空字符的功能
 */
JS.trim=function(str) {
    if(typeof(str)=='string') {
        if(String.prototype.trim!=undefined) {
            return str.trim();
        }
        return str.replace(/^[\s]+/,'').replace(/[\s]+$/,'');
    }
    return str;
}
/**
 * 获取对象类型
 */
JS.type=function(obj) {
    var _type=Object.prototype.toString.apply(obj);
    return JS.trim(_type.replace(/(\[|\]|(object))/g,''));
}
/**
 * 判断给定的对象是否是有效的html节点(Node)
 */
JS.isNode=function(node) {
    return (typeof(node)=='object'&&node!==null&&(JS.type(node.nodeType)!=='Undefined'||(node instanceof Node)));/*兼容ie8,ie8中'Node'未定义*/
};
/**
 * 判断任意两个元素是否相等
 * _callBack指示是否为回调调用
 */
JS.equals=function(obj_1,obj_2,_callBack) {
    if(JS.isNode(obj_1)||JS.isNode(obj_2)) {
        return obj_1===obj_2;
    }
    var type_1=JS.type(obj_1),
        type_2=JS.type(obj_2);
    if(type_1!=type_2) {
        return false;
    }
    var isEqual=false,inTypes=true;
    switch(type_1) {
        case 'Null':
        case 'Undefined':
        case 'Number':
        case 'Boolean':
        case 'String':
            isEqual=obj_1===obj_2;
            break;
        case 'Function':
            isEqual = code.compress(obj_1.toString()) == code.compress(obj_2.toString());
            break;
        case 'Date':
        case 'RegExp':
            isEqual=obj_1.toString()==obj_2.toString();
            break;
        default:
            inTypes=false;
            break;
    }
    if(inTypes) {
        return isEqual;
    }
    if(type_1=='Array') {
        if(obj_1.length==obj_2.length) {
            for(var i=0;i<obj_1.length;i++) {
                if(!JS.equals(obj_1[i],obj_2[i])) {
                    return false;
                }
            }
            return true;
        }
        else {
            return false;
        }
    }
    else {
        /*
        var ref=JS.objRef(obj_1);
        var _i=0;
        for (var i in obj_1) {
            if(ref.ref.length>0&&array.has(ref.ref,_i)) {
                if(obj_1[i]!==obj_2[i]) {
                    return false;
                }
            }
            else {
                if(!JS.equals(obj_1[i],obj_2[i])) {
                    return false;
                }
            }
            _i++;
            if(_i==ref.length) {
                break;
            }
        }
        */
        for (var j in obj_1) {
            if(!JS.equals(obj_1[j],obj_2[j])) {
                return false;
            }
        }
    }
    return true;
};
/**
 * 对象拷贝
 */
JS.clone=function(obj) {
    if(JS.isNode(obj)) {
        return obj.cloneNode(true);
    }
    var objClone;
    if (obj.constructor == Object) {
        objClone = new obj.constructor();
    } else {
        objClone = new obj.constructor(obj.valueOf());
    }
    for (var key in obj) {
        if ((objClone[key] != obj[key])||(obj[key]===null)) {
            if (typeof (obj[key]) == 'object'&&obj[key]!=null) {
                objClone[key] = arguments.callee(obj[key]);
            } else {
                objClone[key] = obj[key];
            }
        }
    }
    objClone.toString = obj.toString;
    objClone.valueOf = obj.valueOf;
    return objClone;
};
/**
 * 函数: 数组或JSON对象的深层拷贝
 * 参数: 
 *    obj: 要拷贝的对象
 *    _callBack: 指示是否为回调调用
 * 返回: 拷贝的对象
*/
JS.deepCopy = function (obj,_callBack) {
    if (obj===null||obj===undefined) {
        return obj;
    }
    var ret=undefined;
    var isIn=true;
    switch(JS.type(obj)) {
        case 'Number':
        case 'Boolean':
        case 'String':
            ret=obj;
            break;
        case 'Date':
            ret=new Date(obj.valueOf());
            break;
        case 'RegExp':
            var flags = [];
            flags.push(obj.global ? 'g' : '');
            flags.push(obj.ignoreCase ? 'i' : '');
            flags.push(obj.multiline ? 'm' : '');
            ret=new RegExp(obj.source, flags.join(''));
            break;
        default:
            isIn=false;
            break;
    }
    if(isIn) {
        return ret;
    }
    if (obj instanceof Array) {
        var n = [];
        for (var i = 0; i < obj.length; ++i) {
            n.push(JS.deepCopy(obj[i]));
        }
        return n;
    } 
    else {
        /*
        var n = {};
        var ref=JS.objRef(obj);
        var _i=0;
        for (var i in obj) {
            if(ref.ref.length>0&&array.has(ref.ref,_i)) {
                n[i]=obj[i];
            }
            else {
                n[i] = JS.deepCopy(obj[i]);
            }
            _i++;
            if(_i==ref.length) {
                break;
            }
        }
        */
        return JS.clone(obj);
    }
};
/**
 * 判断JS对象个数和是否存在循环引用
 */
JS.objRef=function(obj) {
    var _count=0;
    var _ref=[];
    for(var i in obj) {
        for(var ii in obj[i]) {
            if(obj[i][ii]===obj) {
                _ref.push(_count);
            }
        }
        _count++;
    }
    return {
        length: _count,
        ref: _ref
    };
};
/**
 * 字符串转JSON
 */
JS.str2json=function(str) {
    var _json=undefined;
    try {
        _json=JSON.parse(str);
    }
    catch(e) {
        _json=eval(['(',str,')'].join(''));
    }
    return _json;
};
/**
 * JSON转字符串
 */
/**
 * _callBack指示是否为回调调用
 */
JS.json2str=function(json,_callBack) {
    var _str=undefined;
    if(json==null) {
        return _str;
    }
    try {
        _str=JSON.stringify(json);
    }
    catch(e) {
        _str='{';
        for(var item in json) {
            var _value=json[item];
            if(typeof(_value)=='object'&&_value!=null) {
                _value=JS.json2str(_value,true);
            }
            if(_value==null) {
                _value='null';
            }
            else if(!isNaN(_value)) {
                _value=_value.toString();
            }
            else if((/^[{][\s\S]+[}]$/g).test(_value)) {}
            else {
                _value=['"',_value,'"'].join('');
            }
            _str=[_str,'"',item,'":',_value,','].join('')
        }
        if(_str.length>1) {
            _str=_str.substr(0,_str.length-1);
        }
        _str=[_str,'}'].join('');
    }
    return _str;
};
/**
 * (初略)判断给定的参数是否为json
 */
JS.isJson=function(json) {
    var strJson=JS.json2str(json);
    if(strJson==undefined) {
        return false;
    }
    var _lc=strJson.match(/[{]/g);
    var _rc=strJson.match(/[}]/g);
    var _qc=strJson.match(/["]/g);
    if(_lc==null||_rc==null||_qc==null) {
        return false;
    }
    if(_lc.length!=_rc.length||_qc.length%2!=0) {
        return false;
    }
    return (/^[{]["][a-zA-Z]\w+["][:]/).test(strJson)&&(/[}]$/).test(strJson);
};
/**
 * 函数: 更新用给定的JSON对象修改源JSON对象, 覆盖源JSON对象中的同名属性 
 * 参数: sourceJSON: 源JSON对象, supplyJSON: 给定的JSON对象, isModifySource: 是否修改源对象[只有此参数为true是才会修改源JSON对象]
 */
JS.updateJSON = function (sourceJSON, supplyJSON, isModifySource) {
  var tempJSON;
  if (typeof(isModifySource) != 'undefined' && isModifySource === true) {
    tempJSON = sourceJSON;
  } 
  else {
    tempJSON = JS.deepCopy(sourceJSON);
  }
  for (var dp in tempJSON) {
    if (supplyJSON.hasOwnProperty(dp)) {
      tempJSON[dp] = supplyJSON[dp];
    }
  }
  return tempJSON;
};
/**
 * 十进制数转化为二进制形式显示(字符串)
 */
JS.bin=function(num) {
    if(typeof(num)!='number') {
        console.error('parameter "num" should be a effective number');
        return undefined;
    }
    return JS.other(num,2);
};
/**
 * 十进制数转化为八进制形式显示(字符串)
 */
JS.otc=function(num) {
    if(typeof(num)!='number') {
        console.error('parameter "num" should be a effective number');
        return undefined;
    }
    return JS.other(num,8);
};
/**
 * 其他进制表示的数(字符串)转化为十进制数
 */
JS.dec=function(strNum,radix) {
    if((/^[A-Z0-9a-z]+$/).test(strNum)==false) {
        console.error('parameter "strNum" is illegal');
        return undefined;
    }
    if(typeof(radix)!='number'||radix<2||radix>36) {
        console.error('parameter "radix" is illegal, "radix" should be a intdger between 2 and 36');
        return undefined;
    }
    return parseInt(strNum,radix);
};
/**
 * 十进制数转化为十六进制形式显示(字符串)
 */
JS.hex=function(num) {
    if(typeof(num)!='number') {
        console.error('parameter "num" should be a effective number');
        return undefined;
    }
    return JS.other(num,16);
};
/**
 * 将十进制数转化为其它进制显示(字符串)
 */
JS.other=function(num,radix) {
    if(typeof(num)!='number') {
        console.error('parameter "num" should be a effective number');
        return undefined;
    }
    if(typeof(radix)!='number'||radix<2||radix>36) {
        console.error('parameter "radix" is illegal, "radix" should be a intdger between 2 and 36');
        return undefined;
    }
    var str = [];
    var _integer = num;
    var _mod = 0;
    while (_integer >= radix) {
        _mod = parseInt(_integer % radix);
        _integer = parseInt(_integer / radix);
        str.push(JS._num2char(_mod));
    }
    str.reverse();
    str.push(JS._num2char(_integer));
    str.reverse();
    return str.join('');
};
/**
 * 将数字(0~35)转化为大进制的单个字符显示(仅供内部调用)
 */
JS._num2char=function(num) {
    var _tmp=undefined;
    if(num>=0&&num<=9) {
        _tmp=num.toString();
    }
    else if(num>=10&&num<=35) {
        _tmp=String.fromCharCode(num+65-10);
    }
    return _tmp;
};
/**
 * 控制台包装
 */
;(function() {
    JS.log_on=true;
    JS.warn_on=true;
    JS.error_on=true;
    JS.dir_on=true;
    if(!window.console) { 
        window.console={
            log: function() {},
            warn: function() {},
            error: function() {},
            dir: function() {}
        };
    }
})();
JS.log=function() {
    if(JS.log_on) {
        //console.log(Array.prototype.slice.call(arguments));
        //console.log.apply(console, arguments);
        Function.prototype.apply.call(console.log, console, arguments);
    }
};
JS.warn=function() {
    if(JS.warn_on) {
        console.warn.apply(console, arguments);
    }
};
JS.error=function() {
    if(JS.error_on) {
        console.error.apply(console, arguments);
    }
};
JS.dir=function() {
    if(JS.dir_on) {
        console.dir.apply(console, arguments);
    }
};

/*****************end of JS.js****************/