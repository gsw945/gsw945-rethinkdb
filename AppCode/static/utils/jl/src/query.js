/*****************begin of query.js***********************/
/**
 * description: a javascript library which provide the function of event bind/unbind(include anymouse-function bind and unbind),simple packge of DOM2 QuerySelecter API,detect the browser version
 */
/**
 * query定义
 */
var query={};
/**
 * 功能: 得到当前的浏览器类型以及版本信息
 * 参数: is_all: 是否只返回主版本号，默认返回主版本号码(数字, 为获取到返回undefined)，仅当此参数为true时返回完整版本号码(字符串)
 */
query.browser = function (is_all) {
  var _name = '',
  _version = '';
  var ua = navigator.userAgent.toLowerCase();
  if (window.ActiveXObject) {
    _name = 'IE';
    try{
        _version = ua.match(/msie ([\d.]+)/i) [1];
    }
    catch(e) {
        _version=ua.match(/([\d.]+)\) like Gecko/i)[1];
    }
  } 
  else if (window.opera) {
    _name = 'Opera';
    _version = ua.match(/opera.([\d.]+)/i) [1];
  } 
  else if (window.chrome != undefined) {
    _name = 'Chrome';
    _version = ua.match(/chrome\/([\d.]+)/i) [1];
  } 
  else if (window.defaultStatus == undefined) {
    _name = 'Firefox';
    _version = ua.match(/firefox\/([\d.]+)/i) [1];
  } 
  else if (window.openDatabase != undefined) {
    _name = 'Safari';
    _version = ua.match(/version\/([\d.]+)/i) [1];
  }
  if(is_all!=true) {
    if((/^\d+[\d.]*$/).test(_version)) {
        _version=parseInt(_version);
    }
    else {
        _version=undefined;
    }
  }
  return {name: _name,version: _version};
};
/**
 * 对象拷贝
 */
query.clone=function(obj) {
    return JS.deepCopy(obj);
};
/**
 * 功能: 元素查询(第一个匹配)
 * 参数: selecter: css选择符, obj: 未设置时默认为document
 */
query.one=function(selecter,obj) {
    if((typeof obj)!='object'||obj==null) {
        obj=window.document;
    }
    if(typeof selecter!='string') {
        console.error('parameter "selecter" should be a CSS selectors with string type');
        return;
    }
    return obj.querySelector(selecter);
};
/**
 * 功能: 元素查询(所有匹配)
 * 参数: selecter: css选择符, obj: 未设置时默认为document
 * 返回: nodeList集合
 */
query.all=function(selecter,obj) {
    if((typeof obj)!='object'||obj==null) {
        obj=window.document;
    }
    if(typeof selecter!='string') {
        console.error('parameter "selecter" should be a CSS selectors with string type');
        return;
    }
    return obj.querySelectorAll(selecter);
};
/**
 * 功能: document.getElementById的简写
 */
query.id=function(strId) {
    if(typeof(strId)=="string"&&strId.length>0) {
        return document.getElementById(strId);
    }
    return undefined;
};
/**
 * 获取或设置节点属性(当参数 attrValue 未设置时为获取，设置时为设置)
 */
query.attr=function(elem,attrName,attrValue) {
    if(!query.isNode(elem)) {
        console.error('parameter "elem" should be a effective html Node');
        return undefined;
    }
    if(typeof(attrName)=="string"&&attrName.length>0) {
        if((typeof(attrValue)=="string")||(typeof(attrValue)=='number')) {
            elem.setAttribute(attrName,attrValue);
        }
        else {
            return elem.getAttribute(attrName);
        }
    }
    return undefined;
};
/**
 * 获取DOM元素的直接父级元素
 */
query.parent=function(elem) {
    if(!query.isNode(elem)) {
        console.error('parameter "elem" should be a effective html Node');
        return undefined;
    }
    var _parent=elem.parentNode;
    return _parent==undefined?elem.parentElement:_parent;
};
/**
 * 获取DOM元素的前一个兄弟元素
 */
query.prev=function(elem) {
    if(!query.isNode(elem)) {
        console.error('parameter "elem" should be a effective html Node');
        return undefined;
    }
    return elem.previousSibling; 
};
/**
 * 获取DOM元素的下一个兄弟元素
 */
query.next=function(elem) {
    if(!query.isNode(elem)) {
        console.error('parameter "elem" should be a effective html Node');
        return undefined;
    }
    return elem.nextSibling; 
};
/**
 * 获取(或者设置)元素的html源码，或者将字符串包装成 DocumentFragment
 */
query.html=function(elem,val) {
    if(elem==undefined||elem==null) {
        console.error('parameter "elem" should be a effective html Node or a string to transform into Node or DocumentFragment');
        return undefined;
    }
    if(query.isNode(elem)) {
        if(typeof(val)=='string') {
            elem.innerHTML='';
            query.append(elem,val);
        }
        else {
            return elem.innerHTML;
        }
    }
    else if(typeof(elem)=='string') {
        var _tmp=document.createElement('div');
        _tmp.innerHTML=elem;
        var ret=query._trans(_tmp.childNodes);
        _tmp.innerHTML='';
        _tmp.appendChild(ret);
        if(JS.type(ret)!=='Undefined'&&ret!==null) {
            ret=_tmp.childNodes;
            if(ret.length==1) {
                return ret[0];
            }
            else {
                return ret;
            }
        }
    }
    return undefined;
};
/**
 * 移除html代码中的html标签
 */
query.removeTag=function(html) {
    if(typeof(html)!='string') {
        console.error('parameter "html" should be a string whit html code');
        return undefined;
    }
    return html.replace(/<[a-z\d]+[^>]*[\w\-]+\=('|")[^'"]*>[^'"]*('|")[^>]*>/gi,'').replace(/<[a-z\d]+[^>]*?>/gi,'').replace(/<\/[a-z\d]+>/gi,'').replace(/<[a-z\d]+\x20\/>/gi,'');
};
/**
 * 获取(或者设置)元素的文本，或者将字符串包装成文本节点
 */
query.text=function(elem,val) {
    if(elem==undefined||elem==null) {
        console.error('parameter "elem" should be a effective html Node or a string to transform into TextNode');
        return undefined;
    }
    if(query.isNode(elem)) {
        if(typeof(val)=='string') {
            if(elem.value==undefined) {
                elem.innerHTML=val.replace(/\</g,'&lt;').replace(/\>/g,'&gt;');
            }
            else {
                elem.value=val;
            }
        }
        else {
            var _text;
            if(typeof(elem.value)=='string') {
                _text=elem.value;
            }
            else {
                var _bro=query.browser();
                if(_bro.name=='IE'&&_bro.version<9) {
                    _text=query.removeTag(elem.innerHTML);
                }
                else {
                    _text=elem.textContent;
                }
            }
            return _text;
        }
    }
    else if(typeof(elem)=='string') {
        return document.createTextNode(elem);
    }
    return undefined;
};
/**
 * 函数: 移除html元素
 */
query.remove = function(selfElement) {
  var _self = undefined;
  switch ((typeof selfElement).toLowerCase()) {
    case 'string':
      /*若传入的是id*/
      _self = query.all(selfElement);
      break;
    case 'object':
      /*若传入的是DOM对象*/
      _self = selfElement;
      break;
    default:
      console.error('Error: the Object or Id of HtmlElement you provided is Error');
      break;
  }
  if (JS.type(_self)==='Undefined'||_self===null) {
    return;
  }
  if(query.isNodeList(_self)) {
      for(var i=0;i<_self.length;i++) {
        return query.remove(_self[i]);
      }
  }
  if(query.isNode(_self)) {
    _self.parentNode.removeChild(_self);
  }
};

/**
 * 判断给定的对象是否是有效的html元素(Element)
 */
query.isElement=function(elem) {
    return (typeof(elem)=='object'&&elem!==null&&(elem instanceof Element));
};
/**
 * 判断给定的对象是否是Function实例
 */
query.isFunction=function(elem) {
    return (typeof(elem)=='object'&&elem!==null&&(elem instanceof Function));
};
/**
 * 判断给定的对象是否是有效的html节点(Node)
 */
query.isNode=function(node) {
    return JS.isNode(node);
};
/**
 * 判断给定的对象是否是有效的html元素集合(NodeList)
 */
query.isNodeList=function(nodeList) {
    return (typeof(nodeList)=='object'&&nodeList!=null&&(isNaN(nodeList.length)==false)&&((nodeList instanceof NodeList)||(nodeList instanceof HTMLCollection)));
};

/**
 * 节点添加节点(可以是单一节点、节点集合和html源代码)
 */
query.append=function(elem,objChild) {
    if(!query.isElement(elem)) {
        console.error('parameter "elem" is not a effective Element instance');
        return;
    }
    if(objChild==undefined||objChild==null) {
        console.error('parameter "objChild" is undefined or null');
        return;
    }
    if(query.isNode(objChild)) {
        elem.appendChild(objChild);
    }
    else if(query.isNodeList(objChild)) {
        elem.appendChild(query._trans(objChild));
    }
    else if(typeof(objChild)=='string') {
        var _tmp=document.createElement('div');
        _tmp.innerHTML=objChild;
        elem.appendChild(query._trans(_tmp.childNodes));
    }
    else {
        console.error('parameter "objChild" can only be Node、NodeList or html code');
    }
};
/**
 * 转化节点(多个节点集合, 仅供内部使用,未验证参数合法性)(script必须转化，否则没有效果)
 */
query._trans=function(nodes) {
    var _fragment=document.createDocumentFragment()
    for(var i=0;i<nodes.length;i++) {
        _fragment.appendChild(nodes[i].cloneNode(true));
    }
    var _script=query.all('script',_fragment);
    var _html=[];
    for(var j=0;j<_script.length;j++) {
        _html.push({source: _script[j].innerHTML,src: _script[j].src});
        _fragment.removeChild(_script[j]);
    }
    for(var k=0;k<_html.length;k++) {
        var scriptElem=document.createElement('script');
        scriptElem.type="text/javascript";
        if((/\w+/).test(_html[k].src)) {
            scriptElem.src=_html[k].src;
        }
        else if((/\w+/).test(_html[k].source)) {
            scriptElem.innerHTML=_html[k].source;
        }
        else {
            continue;
        }
        _fragment.appendChild(scriptElem);
    }
    return _fragment;
};
/**
 * 动态加载script
 */
query.script=function(url,callBack) {
    var scriptElem=document.createElement('script'),
        _done=false;
    scriptElem.type="text/javascript";
    scriptElem.src=url;
    scriptElem.onload = scriptElem.onreadystatechange = function() {
        if (!_done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete'))
        {
            _done = true;
            if(JS.type(callBack)==='Function') {
                callBack();
            }
            // Handle memory leak in IE
            scriptElem.onload = scriptElem.onreadystatechange = null;
            scriptElem.parentNode.removeChild(scriptElem);
        }
    };
    document.body.appendChild(scriptElem);
};

/**
 * 函数: 通过className得到DOM对象
 * _className: 样式类名, _tag: 标签名[可选], _parent: 父级DOM对象[可选]
 */
query.className=function(_className, _tag, _parent) {
  if (_className == undefined || (_className + '').length < 1) {
    console.error('parameter "className" is required');
    return undefined;
  }
  if (_tag == undefined) {
    _tag = '*';
  }
  if (_parent == undefined) {
    _parent = window.document;
  }
  _className = _className.replace(/\-/g, '_');
  var regex = new RegExp('(^|\\b)' + _className.replace(/\-/g, '_') + '(\\b|$)');
  var classElements = [];
  var tagElements = _parent.getElementsByTagName(_tag);
  for (var _element in tagElements) {
    var cla=tagElements[_element].className;
    if(cla!=undefined&&cla.length>0) {
        cla=cla.replace(/\-/g, '_');
    }
    if (regex.test(cla)) {
      classElements.push(tagElements[_element]);
    }
  }
  return classElements;
};
/**
 * 判断DOM对象是否有指定的类样式
 */
query.hasClass=function(elem,_className) {
    if(!query.isElement(elem)) {
        console.error('parameter "elem" is not a effective Element instance');
        return undefined;
    }
    if(typeof(_className)!='string'||_className.length<1) {
        console.error('parameter "_className" should be a string with length geater than 0');
        return undefined;
    }
    var _class=query.attr(elem,'class');
    if(JS.type(_class)!=='Undefined'&&_class!==null&&_class.length>0) {
        var regex = new RegExp('(^|\\b)' + _className.replace(/\-/g, '\\-') + '(\\b|$)');
        if (regex.test(_class)) {
            return true;
        }
    }
    return false;
};
/**
 * 为DOM对象添加指定的类样式
 */
query.addClass=function(elem,_className) {
    if(!query.isElement(elem)) {
        console.error('parameter "elem" is not a effective Element instance');
        return undefined;
    }
    if(typeof(_className)!='string'||_className.length<1) {
        console.error('parameter "_className" should be a string with length geater than 0');
        return undefined;
    }
    var _class=query.attr(elem,'class');
    _class=(JS.type(_class)==='Undefined'||_class===null)?'':_class;
    query.attr(elem,'class',[_class,(_class.length>0?' ':''),_className].join(''));
};
/**
 * 为DOM对象移除指定的类样式
 */
query.removeClass=function(elem,_className) {
    if(!query.isElement(elem)) {
        console.error('parameter "elem" is not a effective Element instance');
        return undefined;
    }
    if(typeof(_className)!='string'||_className.length<1) {
        console.error('parameter "_className" should be a string with length geater than 0');
        return undefined;
    }
    var _class=query.attr(elem,'class');
    _class=_class==null?'':_class;
    var regex = new RegExp('(^|\\b)' + _className.replace(/\-/g, '\\-') + '(\\b|$)','g');
    _class=JS.trim(_class.replace(regex,'').replace(/[ ]+/g,' '));
    query.attr(elem,'class',_class);
};

/**
 * 获取或设置元素的css属性
 */
query.css=function(elem,name,value) {
    if(!query.isElement(elem)) {
        console.error('parameter "elem" is not a effective Element instance');
        return undefined;
    }
    var style=undefined;
    var likeArr=false;/*是否可以像数组一样操作*/
    if(document.defaultView.getComputedStyle) {
        style=document.defaultView.getComputedStyle(elem, null);
    }
    else if(window.getComputedStyle) {
        style=window.getComputedStyle(elem, null)
    }
    else {
        style=elem.currentStyle;
        likeArr=true;
    }
    if(typeof(name)!='string'||name.length<1) {
        //console.error('parameter "name" should be a string with length geater than 0');
        return undefined;
    }
    if(typeof(value)!='string'||value.length<1) {
        if(style!=undefined) {
            return likeArr?style[name]:style.getPropertyValue(name);
        }
        else {
            console.error("can't read the property of style");
        }
    }
    else {
        elem.style[name]=value;
    }
}
/**
 * 批量设置元素的css属性
 */
query.mulitCss=function(elem,values) {
    if(!JS.isJson(values)) {
        console.error('parameter "values" must be a Json  Object');
        return;
    }
    for(var item in values) {
        query.css(elem,item,values[item]);
    }
};

/**
 * 将用 - 连接的css属性转换为驼峰命名的DOM Style
 */
query.toCamel=function(name) {
    var arr=name.split('-');
    var result=[];
    for(var i=0;i<arr.length;i++) {
        if(arr[i]=='float') {
            result.push('cssFloat');
            continue;
        }
        var len=arr[i].length;
        if(len>0) {
            if(i>0) {
                result.push(arr[i].substr(0,1).toUpperCase()+arr[i].substr(1,len-1));
            }
            else {
                result.push(arr[i]);
            }
        }
    }
    return result.join('');
}

/**
 * 得到DOM元素在兄弟节点中的排序索引
 */
query.index=function(elem) {
    if(!query.isNode(elem)) {
        console.error('parameter "elem" should be a effective html Node');
        return undefined;
    }
    var _parent=query.parent(elem);
    var _sibling=_parent.childNodes;
    var _index=0;
    for(var i=0;i<_sibling.length;i++) {
        if(_sibling[i].nodeType==elem.nodeType) {
            if(_sibling[i]==elem) {
                break;
            }
            else {
                _index++;
            }
        }
    }
    return _index;
};
/**
 * 在元素 targetElem 后面插入元素 newElem
 */
query.insertAfter = function (newElem, targetElem) {
  var parentElem = targetElem.parentNode;
  if(parentElem.lastChild == targetElem)
  {
    parentElem.appendChild(newElem);
  } else {
    parentElem.insertBefore(newElem,targetElem.nextSibling);
  }
};
/**
 * 在元素 targetElem 前面插入元素 newElem
 */
query.insertBefore = function (newElem, targetElem) {
    var parentElem = targetElem.parentNode;
    parentElem.insertBefore(newElem,targetElem);
};
/**
  * 将元素targetElem替换为newElem
  */
query.replace=function(newElem,targetElem) {
    query.insertBefore(newElem,targetElem);
    query.remove(targetElem);
};

/**
 * 获取或设置top
 */
query.top=function(elem,val) {
    if(query.isElement(elem)==false) {
        console.error('parameter "elem" should be a effective html Element');
        return undefined;
    }
    if(typeof(val)=='number') {
        elem.style.top=[val,'px'].join('');
    }
    else {
        var y=elem.offsetTop;
        while(elem=elem.offsetParent) {
            y+=elem.offsetTop;
        }
        return y;
    }
    return undefined;
};
/**
 * 获取或设置left
 */
query.left=function(elem,val) {
    if(query.isElement(elem)==false) {
        console.error('parameter "elem" should be a effective html Element');
        return undefined;
    }
    if(typeof(val)=='number') {
        elem.style.left=[val,'px'].join('');
    }
    else {
        var x=elem.offsetLeft;
        while(elem=elem.offsetParent) {
            x+=elem.offsetLeft
        }
        return x;
    }
    return undefined;
};
/**
 * 获取或设置width
 */
query.width=function(elem,val) {
    if(query.isElement(elem)==false) {
        console.error('parameter "elem" should be a effective html Element');
        return undefined;
    }
    if(typeof(val)=='number') {
        elem.style.width=[val,'px'].join('');
    }
    else {
        return elem.offsetWidth;
    }
    return undefined;
};
/**
 * 获取或设置height
 */
query.height=function(elem,val) {
    if(query.isElement(elem)==false) {
        console.error('parameter "elem" should be a effective html Element');
        return undefined;
    }
    if(typeof(val)=='number') {
        elem.style.height=[val,'px'].join('');
    }
    else {
        return elem.offsetHeight;
    }
    return undefined;
};
/**
 * 获取或设置scrollTop
 */
query.scrollTop=function(val,elem) {
    var _objElem;
    if(query.isElement(elem)==false) {
        if(document.documentElement.scrollTop!=undefined) {
          _objElem=document.documentElement;
        }
        else {
            _objElem=document.body;
        }
    }
    else {
        _objElem=elem;
    }
    if(typeof(val)=='number') {
        _objElem.scrollTop=val;
    }
    else {
        return _objElem.scrollTop;
    }
    return undefined;
};
/**
 * 获取或设置scrollLeft
 */
query.scrollLeft=function(val,elem) {
    var _objElem;
    if(query.isElement(elem)==false) {
        if(document.documentElement.scrollLeft!=undefined) {
          _objElem=document.documentElement;
        }
        else {
            _objElem=document.body;
        }
    }
    else {
        _objElem=elem;
    }
    if(typeof(val)=='number') {
        _objElem.scrollLeft=val;
    }
    else {
        return _objElem.scrollLeft;
    }
    return undefined;
};
/**
 * 获取scrollWidth
 */
query.scrollWidth=function(val,elem) {
    var _objElem;
    if(query.isElement(elem)==false) {
        if(document.documentElement.scrollWidth!=undefined) {
          _objElem=document.documentElement;
        }
        else {
            _objElem=document.body;
        }
    }
    else {
        _objElem=elem;
    }
    return _objElem.scrollWidth;
};
/**
 * 获取或设置scrollHeight
 */
query.scrollHeight=function(val,elem) {
    var _objElem;
    if(query.isElement(elem)==false) {
        if(document.documentElement.scrollHeight!=undefined) {
          _objElem=document.documentElement;
        }
        else {
            _objElem=document.body;
        }
    }
    else {
        _objElem=elem;
    }
    return _objElem.scrollHeight;
};
/**
  * 返回浏览器可视区域宽高
  */
query.view=function() {
    var _w=0,
        _h=0;
    if(JS.type(document.childNodes[0])==='DocumentType') {
        var brow=query.browser();
        if(brow.name=='IE') {
            _w=document.documentElement.clientWidth;
            _h=document.documentElement.clientHeight;
        }
        else {
            _w=window.innerWidth;
            _h=window.innerHeight;
        }
    }
    else {
        _w=document.body.offsetWidth||document.body.clientWidth;
        _h=document.body.offsetHeight||document.body.clientHeight;
    }
    return {
        width: _w,
        height: _h
    };
};
/**
  * 元素nodeA 是否包含元素nodeB
  */
query.contains=function(nodeA, nodeB){
    return nodeA.contains ? (nodeA!=nodeB&&nodeA.contains(nodeB)) : ((nodeA.compareDocumentPosition(nodeB)&16)==16);
}