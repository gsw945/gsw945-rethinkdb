var http = {};

/**
 * 发送ajax请求
 */
http._request = function(type,url,data,callBack) {
  var xhr=undefined;
  if(window.XMLHttpRequest) {
    xhr=new XMLHttpRequest();
  }
  else {
    try {
      xhr=new ActiveXObject('Msxml2.XMLHTTP');
    }
    catch(e) {
      xhr=new ActiveXObject('Microsoft.XMLHTTP');
    }
  }
  xhr.onreadystatechange = function() {
    if(xhr.readyState==4) {
      if(xhr.status==200) {
        callBack(xhr.responseText);
      }
      else {
        if(xhr.status==404) {
          console.log('404');
        }
        console.log(xhr.statusText);
      }
    }
  };
  /*防止浏览器强加www子域，导致请求跨域*/
  if(location.href.indexOf('www',0)!=-1&&url.indexOf('www',0)==-1) {
    url=url.replace('://','://www.');
  }
  var tmp;
  if(type.toLowerCase()=='post') {
      xhr.open('POST',url,true);
      xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      tmp=http.json2paras(data);
      xhr.send(tmp);
  }
  else if(type.toLowerCase()=='put') {
      xhr.open('PUT',url,true);
      // xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
      xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      tmp=http.json2paras(data);
      xhr.send(tmp);
  }
  else if(type.toLowerCase()=='delete') {
      xhr.open('DELETE',url,true);
      // xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
      xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
      tmp=http.json2paras(data);
      xhr.send(tmp);
  }
  else {
      tmp=http.json2paras(data);
      if(tmp!=null) {
          if(url.indexOf('?',0)!=-1) {
              url=url+'&'+tmp;
          }
          else {
              url=url+'?'+tmp;
          }
      }
      xhr.open('GET',url,true);
      xhr.send();
  }
}
/**
 * 通过POST方式提交数据
 */
http.post = function(url,data,callBack) {
  http._request('POST',url,data,callBack);
}
/**
 * 通过GET方式提交数据
 */
http.get = function(url,data,callBack) {
  http._request('GET',url,data,callBack);
}
/**
 * 通过PUT方式提交数据
 */
http.put = function(url,data,callBack) {
  http._request('PUT',url,data,callBack);
}
/**
 * 通过DELETE方式提交数据
 */
http.delete = function(url,data,callBack) {
  http._request('DELETE',url,data,callBack);
}
/**
 * jsonp
 */
http.jsonp = function(url,key,callBack) {
    var _script=query.html(['<script type="text/javascript" src="',url,'?',key,'=',callBack,'"></script>'].join(''));
    query.append(document.body,_script);
    bind.addEvent('load',function() {
        console.log(_script);
        query.remove(_script);
    },_script);
}
/*
jsonp-demo:
js:
function log_msg(data) {
    console.log(data);
}
http.jsonp('<url>','callback','log_msg');

php:
$callback=$_GET['callback'];
echo $callback.'('.json_encode(array('name'=>'zhagnsan','age'=>12)).');';
*/
/**
 * 将json对象转化为uri参数
 */
http.json2paras = function(data) {
    var tmp=null;
    if(typeof(data)=='object') {
        if(data!=null) {
            tmp='';
            for(var item in data) {
                var _val;
                if(JS.type(data[item])==='Undefined'||data[item]===null) {
                    _val='';
                }
                else {
                    _val=JS.type(data[item])==='Object'?http.specialEnCode(JS.json2str(data[item])):http.specialEnCode(data[item]);
                }
                tmp=[tmp,item,'=',_val,'&'].join('');
            }
            if(tmp.length>0) {
                tmp=tmp.substr(0,tmp.length-1);
            }
        }
    }
    tmp=tmp==null?data:tmp;
    if(tmp!=null) {
        tmp=encodeURI(tmp);
    }
    return tmp;
}
/**
 * 将特殊字符['&','"',"'",'#','%','+','/','=','?','<','>']编码
 */
http.specialEnCode = function(source) {
    var ret;
    switch(JS.type(source)) {
        case 'Number':
        case 'Boolean':
        case 'Array':
        case 'Date':
        case 'RegExp':
            source=source.toString();
        case 'String':
            ret=source.replace(/([&])|(["])|(['])|([#])|([%])|([+])|([/])|([=])|([?])|([<])|([>])/g,function(match) {
                return ['%',JS.other(match.charCodeAt(0),16)].join('');
            });
            break;
        default:
            ret=source;
            break;
    }
    return ret;
}
/**
 * 将特殊字符解码(功能不能用系统函数 decodeURI 替代)
 */
http.specialDeCode = function(source) {
    if(typeof(source)==='string') {
        return source.replace(/[%]((26)|(22)|(27)|(23)|(25)|(2b)|(2f)|(3d)|(3f)|(3c)|(3e))/ig,function(match) {
            return String.fromCharCode(JS.dec(match.replace(/[%]/g,''),16));
        });
    }
    return source;
}