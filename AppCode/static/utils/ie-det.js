function maybeIE() {
    var ua = navigator.userAgent.toLowerCase();
    var _v = NaN;
    var _ie = false;
    if (window.ActiveXObject) {
        _ie = true;
        try{
            _v = ua.match(/msie ([\d.]+)/i) [1];
        }
        catch(e) {
            _v=ua.match(/([\d.]+)\) like Gecko/i)[1];
        }
    }
    if(_ie) {
        if(!isNaN(_v)) {
            _v = parseInt(_v);
        }
    }
    return {
        'ie': _ie,
        'version': _v
    };
}