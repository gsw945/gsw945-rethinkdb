function saveArticle(_title, _content, _summary) {
    var txt_id = query.one('#txt-id');
    var _url = '/api/article/',
        _method = http.post;
    if(txt_id) {
        var _id = txt_id.value;
        _url = '/api/article/' + _id,
        _method = http.put;
    }
    _method(
        _url,
        {
            title: _title,
            content: _content,
            summary: _summary
        },
        function(data) {
            console.log(data);
            iModal({
                title:'提示',
                content:'保存成功',
                ok:function(){
                    console.log('save successed')
                }
            });
        }
    );
}

var simplemde = new SimpleMDE({
    element: document.getElementById("article-editor"),
    autoDownloadFontAwesome: false,
    spellChecker: false,
    tabSize: 4,
    indentWithTabs: false,
    lineWrapping: false,
    showIcons: ["code", "table"],
    toolbar: [
        'bold', 'italic', 'strikethrough',
        'heading', 'heading-smaller', 'heading-bigger', 'heading-1', 'heading-2', 'heading-3',
        'code', 'quote', 'unordered-list', 'ordered-list', 'clean-block', 'link', 'image', 'table', 'horizontal-rule',
        'preview', 'side-by-side', 'fullscreen', 'guide',
        {
            name: "save",
            action: function customFunction(editor){
                // Add your own code
                var title = query.one('#txt-title').value;
                var val = editor.value();
                var summary = query.one('#txt-summary').value;
                saveArticle(title, val, summary);
            },
            className: "fa fa-save",
            title: "save",
        }
    ],
    autofocus: true,
    autosave: {
        enabled: false,
        uniqueId: "gsw945-editor",
        delay: 1000
    },
    renderingConfig: {
        codeSyntaxHighlighting: true
    },
    codeBlockModes: ['javascript', 'python', 'css'],
    previewRender: function(plainText, preview) {
        setTimeout(function() {
            preview.innerHTML = this.parent.markdown(plainText);
            var codes = query.all('pre', preview);
            if(codes && codes.length > 0) {
                for (var i = 0; i < codes.length; i++) {
                    query.addClass(codes[i], 'line-numbers');
                }
            }
            Prism.highlightAll();
        }.bind(this), 1);
        return "Loading..."
    },
    promptURLs: true
});

simplemde.codemirror.options.extraKeys.Home = "goLineLeft";
simplemde.codemirror.options.extraKeys.End = "goLineRight";