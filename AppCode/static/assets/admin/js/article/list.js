var base_path = window.GLOBAL.STATIC_BASE,
    current_module = window.GLOBAL.MODULE;
    
loadConfig('debug', false);

// pure.min.css 必须先于grids-responsive*-min.css加载
loadCSS(base_path + 'lib/purecss/pure.min.css');

var _ie = maybeIE();
var lte_ie8 = false;
if(_ie.ie && !isNaN(_ie.version)) {
    var lte_ie8 = _ie.version <= 8;
    if(_ie.version < 9) {
        loadJS(base_path + 'lib/html5shiv/html5shiv.min.js');
        loadJS(base_path + 'lib/ie7-js/IE9.js');
        loadJS(base_path + 'lib/respond/respond.min.js');
        // https://github.com/jonathantneal/flexibility
        loadJS(base_path + 'lib/flexibility/flexibility.js');
        loadJS(base_path + 'lib/classList/classList.min.js');
    }
}
if(_ie.ie && lte_ie8) {
    loadCSS(base_path + 'lib/purecss/grids-responsive-old-ie.min.css');
}
else {
    loadCSS(base_path + 'lib/purecss/grids-responsive.min.css');
}
loadCSS(base_path + 'lib/purecss/pure-extensions.css');

loadCSS(base_path + 'lib/font-awesome/css/font-awesome.min.css');

loadJS(GLOBAL.JL_DIST);

loadCSS(base_path + 'assets/admin/css/top-menu.css');
loadJS(base_path + 'assets/admin/js/top-menu.js');

loadCSS(base_path + 'assets/admin/css/article/list.css');