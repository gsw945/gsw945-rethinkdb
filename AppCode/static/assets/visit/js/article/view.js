var base_path = window.GLOBAL.STATIC_BASE,
    current_module = window.GLOBAL.MODULE;

// pure.min.css 必须先于grids-responsive*-min.css加载
loadCSS(base_path + 'lib/purecss/pure.min.css');

var _ie = maybeIE();
var lte_ie8 = false;
if(_ie.ie && !isNaN(_ie.version)) {
    var lte_ie8 = _ie.version <= 8;
    if(_ie.version < 9) {
        loadJS(base_path + 'lib/html5shiv/html5shiv.min.js');
        loadJS(base_path + 'lib/ie7-js/IE9.js');
        loadJS(base_path + 'lib/respond/respond.min.js');
    }
}
if(_ie.ie && lte_ie8) {
    loadCSS(base_path + 'lib/purecss/grids-responsive-old-ie.min.css');
}
else {
    loadCSS(base_path + 'lib/purecss/grids-responsive.min.css');
}

// loadCSS(base_path + 'utils/top-menu/custom.css');
// loadJS(base_path + 'utils/top-menu/custom.js');

loadCSS([base_path, 'assets/visit/css/article.css'].join(''));

loadCSS(base_path + 'lib/prismjs/prism.css');
loadCSS(base_path + 'assets/admin/css/article/prism-line-number.css');

loadJS(GLOBAL.JL_DIST, function() {
    loadJS(base_path + 'lib/showdown/showdown.min.js', function() {
        var converter = new showdown.Converter({
            tables: true
        });
        var container = query.one('#layout .article-viewer .article-content');
        var text = query.text(query.one('script[type="text/plain"]', container));
        var html = converter.makeHtml(text);
        query.html(container, html);
        var codes = query.all('pre', container);
        if(codes && codes.length > 0) {
            for (var i = 0; i < codes.length; i++) {
                query.addClass(codes[i], 'line-numbers');
            }
        }

        chainLoadJS(
            base_path + 'lib/highlightjs/highlight.js',
            base_path + 'lib/prismjs/prism.js',
            base_path + 'assets/admin/js/article/prism-line-number.js'
        );
    });
});
