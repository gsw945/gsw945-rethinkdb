# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys, traceback

from logbook import Logger, TimedRotatingFileHandler

class LogWrapper(object):
    """docstring for LogWrapper"""
    def __init__(self, log_path):
        super(LogWrapper, self).__init__()
        self.log_path = log_path

    def _record(self, msg, level, name=None):
        handler = TimedRotatingFileHandler(
            '{0}{1}.log'.format(self.log_path, level),
            level=LogWrapper.LEVEL.get(level, 'NOTSET'),
            date_format='%Y-%m-%d'
        )
        handler.push_application()
        if name is None:
            name = 'wkj'
        logger = Logger(name)
        getattr(logger, level)(msg)
        handler.pop_application()

    def trace(self, msg, name=None):
        '''trace'''
        self._record(msg, 'trace', name)

    def debug(self, msg, name=None):
        '''debug'''
        self._record(msg, 'debug', name)

    def info(self, msg, name=None):
        '''info'''
        self._record(msg, 'info', name)

    def notice(self, msg, name=None):
        '''notice'''
        self._record(msg, 'notice', name)

    def warning(self, msg, name=None):
        '''warning'''
        self._record(msg, 'warning', name)

    def error(self, msg, name=None):
        '''error'''
        self._record(msg, 'error', name)

    def critical(self, msg, name=None):
        '''critical'''
        self._record(msg, 'critical', name)

    def exception(self, msg, name=None):
        '''exception'''
        self._record(msg, 'exception', name)

    def getExcept(self):
        exc_type, exc_value, exc_traceback = sys.exc_info()
        return ''.join(traceback.format_exception(exc_type, exc_value, exc_traceback))

    LEVEL = {
        'critical': 'CRITICAL',
        'error': 'ERROR',
        'warning': 'WARNING',
        'notice': 'NOTICE',
        'info': 'INFO',
        'debug': 'DEBUG',
        'trace': 'TRACE',
        'notset': 'NOTSET'
    }

if __name__ == '__main__':
    log = LogWrapper('/home/kz-gsw/log/')
    log.info('hello')