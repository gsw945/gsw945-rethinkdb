# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from . import db_conn

class Article(object):
    """docstring for Article"""
    def __init__(self):
        super(Article, self).__init__()

    def add(self, title, content, summary):
        query, conn = db_conn()
        db_result = query.insert({
            'title': title,
            'content': content,
            'summary': summary
        }).run(conn)
        return db_result
    
    def query(self, _id=None):
        '''query'''
        db_result = None
        query, conn = db_conn()
        if bool(_id):
            obj = query.get(_id).run(conn)
            db_result = {}
            for key in obj:
                db_result[key] = obj[key]
        else:
            cursor = query.run(conn)
            db_result = []
            for document in cursor:
                db_result.append(document)
        return db_result

    def update(self, _id, title, content, summary):
        query, conn = db_conn()
        db_result = query.get(_id).update({
            'title': title,
            'content': content,
            'summary': summary
        }).run(conn)
        return db_result