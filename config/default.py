# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

MAX_CONTENT_LENGTH = 1024 * 1024 * 1024 # 1GB

SESSION_COOKIE_NAME = 'gsw945_websiet_session'

DEBUG = False