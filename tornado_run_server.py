# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop


import run

http_server = HTTPServer(WSGIContainer(run.app))
http_server.listen(run.extra_config.LISTEN_PORT)

IOLoop.instance().start()